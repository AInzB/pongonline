const socket = io();

let speed = 5;
let imLeft = true;
let continueMove = true;

// DOM Elements
var ball = {
    element: document.getElementById('ball'),
    directionH: 'left',
    directionV: 'up'
};

let leftside = document.getElementById('leftSide');
let rigtside = document.getElementById('rightSide');

document.addEventListener('keydown', (event) => {
    if(event.key == "ArrowUp"){
        if(imLeft) moveSideUp(leftside);
        else moveSideUp(rigtside);
    }

    if(event.key == "ArrowDown"){
        if(imLeft) moveSideDown(leftside);
        else moveSideDown(rigtside);
    }

    if(event.key == "Enter"){
        ballAnimation();
    }
});

function moveSideUp(element){
    let actualTop = element.getBoundingClientRect();
    let newPosition = actualTop.top - speed < 25 ? 25 : actualTop.top - speed;
    element.style.top = newPosition + 'px';
}

function moveSideDown(element){
    let actualBot = element.getBoundingClientRect();
    let newPosition = actualBot.bottom + speed > (window.innerHeight - 25) ? window.innerHeight - 25 : actualBot.bottom + speed;
    newPosition -= actualBot.height;
    element.style.top = newPosition + 'px';
}

function moveBallVertical(){
    let positions = ball.element.getBoundingClientRect();
    let newV;

    if(ball.directionV == 'up'){
        if(positions.top - speed < 25){
            ball.directionV = 'down';
            newV = 25;
        }else{
            newV = positions.top - speed;
        }
    }else{
        if(positions.bottom + speed > (window.innerHeight - 25)){
            ball.directionV = 'up';
            newV = window.innerHeight - 25;
            newV -= positions.height;
        }else{
            newV = positions.bottom + speed;
            newV -= positions.height;
        }
    }

    ball.element.style.top = newV + 'px';
}

function moveBallHorizontal(){
    let positions = ball.element.getBoundingClientRect();
    let newH;

    if(ball.directionH == 'left'){
        let posLeftSide = leftside.getBoundingClientRect();
        if(positions.left - speed <= posLeftSide.right && posLeftSide.bottom >= positions.top && posLeftSide.top <= positions.bottom){
            newH = posLeftSide.right + 1;
            ball.directionH = 'right';
        }else if(positions.left - speed <= posLeftSide.right){
            newH = posLeftSide.right;
            continueMove = false;
            alert('Has perdido');
        }else{
            newH = positions.left - speed;
        }
    }else{
        let posRightSide = rigtside.getBoundingClientRect();
        if(positions.right + speed >= posRightSide.left && posRightSide.bottom >= positions.top && posRightSide.top <= positions.bottom){
            newH = posRightSide.left + 1;
            newH -= positions.width;
            ball.directionH = 'left';
        }else if(positions.right + speed >= posRightSide.left){
            newH = posRightSide.left + 1;
            newH -= positions.width;
            continueMove = false;
            alert('Has perdido');
        }else{
            newH = positions.left + speed;
        }
    }

    console.log(ball.directionH)
    ball.element.style.left = newH + 'px';
}

function ballAnimation(timestamp){
    moveBallVertical();
    moveBallHorizontal();

    if(continueMove){
        window.requestAnimationFrame(ballAnimation);
    }
}

// DOM Elements
let message = document.getElementById('message');
let username = document.getElementById('username');
let btnSend = document.getElementById('send');
let output = document.getElementById('output');
let actions = document.getElementById('actions');

btnSend.addEventListener('click', function(){
    socket.emit('chat:message', {
        message: message.value,
        username: username.value
    });
});

message.addEventListener('keypress', function(){
    socket.emit('chat:typing', username.value);
});

socket.on('chat:message', function(data) {
    actions.innerHTML = '';
    output.innerHTML += `
    <p>
        <strong>${data.username}</strong>: ${data.message}
    </p>
    `;
});

socket.on('chat:typing', function(data){
    actions.innerHTML = `<p><em>${data} is typing a message.</em></p>`
});